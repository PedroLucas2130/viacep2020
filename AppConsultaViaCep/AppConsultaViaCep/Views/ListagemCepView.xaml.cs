﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppConsultaViaCep.Views
{
    public class Endereco
    {

        public string Logradouro { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Localidade { get; set; }
        public string Uf { get; set; }

    }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListagemCepView : ContentPage
    {
        public Endereco Endereco { get; set; }
        public ListagemCepView()
        {
            InitializeComponent();
            this.Endereco = new Endereco();



            this.BindingContext = this;
            BuscarEndereco();
        }
        public async void BuscarEndereco()
        {
            Console.WriteLine("Iniciando Consumo da API");

            Uri url = new Uri("https://viacep.com.br/ws/12425020/json/");

            // call endpoint
            HttpResponseMessage httpResponse = await Services.HttpService.GetRequest(url.AbsoluteUri);
            if (httpResponse.IsSuccessStatusCode)
            {
                string stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
                Console.WriteLine("\n==================================================");
                Console.WriteLine(stringResponse);

                Endereco End = Services.SerializationService.DeserializeObject<Endereco>(stringResponse);
                this.Endereco = End;
                OnPropertyChanged(nameof (this.Endereco));


               
            }
        }
    }
}